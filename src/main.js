const footer = document.querySelector(".app-footer")
const mainContainer = document.querySelector("#app-container")

addJoinOurProgramSection()

function addJoinOurProgramSection(){
    const JOPSection = document.createElement("section");
    JOPSection.classList.add("app-section", "app-section--join-our-program");
    /*Creating child elements*/
    const jopTitle = createElement("h1", "app-title", "Join Our Program")
    const jopSubtitle = createElement("h2", "app-subtitle", "Sed do eiusmod tempor incididuntut labore et dolore magna aliqua.")
    const jopForm = createElement("form", "app-form", "")
    const jopInput = createElement("input", "app-form__input")
    jopInput.placeholder = "Email"
    const jopBtn = createElement("button", "app-section__button", "SUBSCRIBE");
    jopBtn.classList.add("app-section__button--subscribe")

    /*Append child elements of form*/
    jopForm.appendChild(jopInput)
    jopForm.appendChild(jopBtn)

    /*Append basic child elements of section*/
    JOPSection.appendChild(jopTitle)
    JOPSection.appendChild(jopSubtitle)
    JOPSection.appendChild(jopForm)
    mainContainer.insertBefore(JOPSection, footer)

    /*Submit event for form*/
    jopForm.addEventListener("submit", (e) => {
        e.preventDefault()
        console.log(jopInput.value)
    })
}

function createElement(tag, className, innerText=""){
   const customElement = document.createElement(tag)
    customElement.classList.add(className)
    customElement.innerText = innerText
    return customElement
}


